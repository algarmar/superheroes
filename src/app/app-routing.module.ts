import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroesListComponent } from './modules/heroes/components/heroes-list/heroes-list.component';

const routes: Routes = [
  {
    path: 'heroes',
    component: HeroesListComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./modules/heroes/heroes.module').then(m => m.HeroesModule)
      }
    ]
  },
  { path: '**', redirectTo: 'heroes' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }