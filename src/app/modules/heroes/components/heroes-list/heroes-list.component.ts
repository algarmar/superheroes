import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Hero } from '../../models/hero';
import { HeroesService } from '../../services/heroes.service';
import { ConfirmDialogComponent } from '../../../shared/components/confirm-dialog/confirm-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.css']
})
export class HeroesListComponent {

  heroes: Array<Hero>;
  columns: Array<string>;

  constructor(
    private heroesService: HeroesService,
    private route: Router,
    public confirmDialog: MatDialog,
    private snackBar: MatSnackBar,
  ) {
    this.heroes = this.heroesService.getAllHeroes().sort((a, b) => b.id - a.id);
    this.columns = ['id', 'name', 'actions'];

    this.route.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
  }

  /**
   * Navegación al formulario de edición de héroe
   * @param hero Héroe
   */
  editHero(hero: Hero) {
    this.route.navigateByUrl(`/heroes/edit-hero/${hero.id}`);
  }

  /**
   * Borrado tras confirmación del héroe
   * @param hero Héroe
   */
  deleteHero(hero: Hero) {
    this.confirmDialog
      .open(ConfirmDialogComponent, {
        data: {
          title: 'Remove hero',
          message: `Do you really want to eliminate ${hero.name}?`,
        }
      })
      .afterClosed()
      .subscribe((confirm: Boolean) => {
        if (confirm) {
          this.heroesService.deleteHero(hero);
          this.snackBar.open('Hero deleted!', null, {
            duration: 3000
          });
          this.route.navigateByUrl('/heroes/heroes-list');
        }
      });
  }
}
