import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroesService } from '../../services/heroes.service';
import { Hero } from '../../models/hero';

@Component({
  selector: 'app-hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.css']
})
export class HeroFormComponent implements OnInit {

  formGroup: FormGroup;
  title: string = 'New hero';
  heroForm = {} as Hero

  constructor(
    private formBuilder: FormBuilder,
    private heroesService: HeroesService,
    private snackBar: MatSnackBar,
    private route: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.createForm();

    if (this.activatedRoute.snapshot.paramMap.get('id')) {
      this.heroForm.id = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
      this.loadHero();
    }
  }

  /**
   * Crea el formulario con el campo nombre
   */
  createForm() {
    this.formGroup = this.formBuilder.group({
      'name': [null, [Validators.required, Validators.minLength(3)]],
    });
  }


  /**
   * Guarda el héroe el servicio
   * @param name Nombre del héroe
   */
  onSubmit({ name: heroName }) {
    this.heroForm.name = heroName;
    this.heroesService.replaceHero(this.heroForm);

    this.snackBar.open('Hero saved!', null, {
      duration: 3000
    });

    this.route.navigateByUrl('/heroes/heroes-list');
  }

  /**
   * Carga los valores en el formulario
   */
  loadHero() {
    const { id } = this.heroForm;
    this.heroForm = this.heroesService.getHeroById(id);

    const { name } = this.heroForm;
    this.formGroup.get('name').setValue(name);
    this.title = `Edit ${name}`;
  }

}
