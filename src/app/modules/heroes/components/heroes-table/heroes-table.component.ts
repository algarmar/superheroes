import { Component, Input, ViewChild, OnInit, Output, EventEmitter } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Hero } from '../../models/hero';

@Component({
  selector: 'app-heroes-table',
  templateUrl: './heroes-table.component.html',
  styleUrls: ['./heroes-table.component.css']
})
export class HeroesTableComponent implements OnInit {

  dataSource: MatTableDataSource<Hero>;

  @Input('filter') filter: boolean = false;
  @Input('data') data: Array<Hero>;
  @Input('displayedColumns') displayedColumns: Array<string>;
  @Output('editHero') editHeroEmitter = new EventEmitter<Hero>();
  @Output('deleteHero') deleteHeroEmitter = new EventEmitter<Hero>();
  @ViewChild(MatPaginator)
  set paginator(value: MatPaginator) {
    this.dataSource.paginator = value;
  }

  constructor(
    private route: Router
  ) { }

  ngOnInit() {
    this.loadData();
  }

  /**
   * Navega hacia el formulario de alta
   */
  newHero() {
    this.route.navigateByUrl('/heroes/new-hero');
  }

  /**
   * Genera el dataSource
   */
  loadData() {
    this.dataSource = new MatTableDataSource<Hero>(this.data);
  }

  /**
   * Filtra el listado de héroes por el parámetro
   * @param filterValue Valor del filtro
   */
  searchHero(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**
   * Lanza el héroe a editar
   * @param hero Héroe
   */
  editHero(hero: Hero) {
    this.editHeroEmitter.emit(hero);
  }

  /**
   * Lanza el héroe a eliminar
   * @param hero Héroe
   */
  deleteHero(hero: Hero) {
    this.deleteHeroEmitter.emit(hero);
  }
}
