import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HeroesTableComponent } from './heroes-table.component';
import { HeroesService } from '../../services/heroes.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { Hero } from '../../models/hero';
import { MaterialModule } from '../../../shared/material.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('HeroesTableComponent', () => {
  let component: HeroesTableComponent;
  let fixture: ComponentFixture<HeroesTableComponent>;
  let heroesService: HeroesService;
  const mockHero: Hero = { id: 1, name: 'Test' };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeroesTableComponent],
      imports: [
        BrowserAnimationsModule,
        MaterialModule,
        RouterTestingModule
      ],
      providers: [HeroesService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroesTableComponent);
    component = fixture.componentInstance;
    heroesService = TestBed.inject(HeroesService);

    component.data = heroesService.ARRAY_HEROES;
    component.filter = true;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe lanzar la función para filtrar un héroe con la palabra "man"', () => {
    spyOn(component, 'searchHero').and.callThrough();
    const { nativeElement: el } = fixture.debugElement.query(By.css('.mat-input-element'));

    el.value = 'man';
    el.dispatchEvent(new Event('keyup'));

    expect(component.searchHero).toHaveBeenCalledWith('man');
  });

  it('Se debe lanzar el evento de edición', () => {
    spyOn(component.editHeroEmitter, 'emit');

    component.editHero(mockHero);

    expect(component.editHeroEmitter.emit).toHaveBeenCalledWith(mockHero);
  });

  it('Se debe lanzar el evento de borrado', () => {
    spyOn(component.deleteHeroEmitter, 'emit');

    component.deleteHero(mockHero);

    expect(component.deleteHeroEmitter.emit).toHaveBeenCalledWith(mockHero);
  });
});
