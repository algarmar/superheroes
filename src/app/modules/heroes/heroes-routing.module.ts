import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroesListComponent } from './components/heroes-list/heroes-list.component';
import { HeroFormComponent } from './components/hero-form/hero-form.component';

const routes: Routes = [
  {
    path: 'heroes',
    children: [
      {
        path: 'heroes-list',
        component: HeroesListComponent
      },
      {
        path: 'new-hero',
        component: HeroFormComponent
      },
      {
        path: 'edit-hero/:id',
        component: HeroFormComponent
      }
    ]
  },
  { path: '**', redirectTo: 'heroes/heroes-list' },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class HeroesRoutingModule { }