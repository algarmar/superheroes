import { Injectable } from '@angular/core';
import { Hero } from '../models/hero';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  ARRAY_HEROES: Array<Hero>;

  constructor() {

    this.ARRAY_HEROES = [
      { id: 1, name: 'Spiderman' },
      { id: 2, name: 'Ironman' },
      { id: 3, name: 'Capitán América' },
      { id: 4, name: 'Thor' },
      { id: 5, name: 'Deadpool' },
      { id: 6, name: 'Viuda Negra' },
      { id: 7, name: 'Hulk' },
      { id: 8, name: 'Doctor Strange' },
      { id: 9, name: 'Lobezno' },
      { id: 10, name: 'Ojo de Halcón' },
      { id: 11, name: 'Tormenta' },
      { id: 12, name: 'El motorista Fantasma' },
      { id: 13, name: 'Jean Grey' },
      { id: 14, name: 'Daredevil' },
      { id: 15, name: 'Falcon' }
    ];
  }

  /**
   * Devuelve todo el listado de héroes
   * @returns Array de héroes
   */
  getAllHeroes(): Array<Hero> {
    return this.ARRAY_HEROES;
  }

  /**
   * Devuelve el héroe por su id
   * @param id id del héroe
   * @returns Hero
   */
  getHeroById(id: number): Hero {
    return this.ARRAY_HEROES.find(hero => hero.id === id) || { id: null, name: null };
  }

  /**
   * Devuelve los héroes que coincidan el nombre pasado por argumento
   * @param name Nombre del héroe
   * @returns Array con los héroes que coincidan con el parámetro
   */
  getHeroByName(name: string): Array<Hero> {
    return this.ARRAY_HEROES.filter(hero => (hero.name.toLowerCase()).includes(name.toLowerCase()));
  }

  /**
   * Actualiza el héroe pasado por argumento o lo añade si es nuevo
   * @param hero Héroe con el nombre actualizado
   * @returns Héroe actualizado
   */
  replaceHero(hero: Hero): Hero {
    let auxHero: Hero;
    const { id, name } = hero;

    if (id) {
      auxHero = this.getHeroById(id);
      auxHero.name = name;
    } else {
      let maxId = this.ARRAY_HEROES.reduce((a, b) => a.id > b.id ? a : b).id || 1;
      auxHero = { id: ++maxId, name: name };
      this.ARRAY_HEROES.push(auxHero);
    }

    return auxHero;
  }

  deleteHero(hero: Hero) {
    const indexHero = this.ARRAY_HEROES.findIndex(auxHero => auxHero.id === hero.id);
    this.ARRAY_HEROES.splice(indexHero, 1);
  }
}
