import { TestBed } from '@angular/core/testing';

import { HeroesService } from './heroes.service';

describe('HeroesService', () => {
  let service: HeroesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HeroesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Debe haber al menos un héroe en el servicio', () => {
    expect(service.ARRAY_HEROES.length).toBeGreaterThan(0);
  });

  it('Debe haber quince héroes', () => {
    expect(service.getAllHeroes().length).toBe(15);
  });

  it('Debe ser el héroe buscado "Ojo de Halcón"', () => {
    const hero = service.getHeroById(10);

    expect(hero.id).toBe(10);
    expect(hero.name).toBe('Ojo de Halcón');
  });

  it('No debe haber un héroe con ID 20', () => {
    const hero = service.getHeroById(20);

    expect(hero.id).toBeNull();
  });

  it('Debe haber dos héroes que contengan la palabra "man"', () => {
    expect(service.getHeroByName('man').length).toBe(2);
  });

  it('Debe actualizarse el héroe con los nuevos valores', () => {
    const updatedSpiderman = { id: 1, name: 'Spiderman modificado' };
    service.replaceHero(updatedSpiderman);

    expect(service.getHeroById(1).name).not.toBe('Spiderman');
  });

  it('Debe borrarse el héroe con id 1', () => {
    service.deleteHero({ id: 1, name: 'Spiderman' });

    expect(service.getHeroById(1).id).toBeNull();
  });
});
