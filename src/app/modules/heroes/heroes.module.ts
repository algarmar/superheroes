import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroesService } from './services/heroes.service';
import { HeroesListComponent } from './components/heroes-list/heroes-list.component';
import { SharedModule } from '../shared/shared.module';
import { HeroesRoutingModule } from './heroes-routing.module';
import { HeroFormComponent } from './components/hero-form/hero-form.component';
import { HeroesTableComponent } from './components/heroes-table/heroes-table.component';
import { MaterialModule } from '../shared/material.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HeroesListComponent,
    HeroFormComponent,
    HeroesTableComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    HeroesRoutingModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  providers: [
    HeroesService
  ]
})
export class HeroesModule { }
