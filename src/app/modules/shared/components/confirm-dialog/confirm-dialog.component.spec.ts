import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ConfirmDialogComponent } from './confirm-dialog.component';
import { By } from '@angular/platform-browser';

describe('ConfirmDialogComponent', () => {
  let component: ConfirmDialogComponent;
  let fixture: ComponentFixture<ConfirmDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConfirmDialogComponent],
      imports: [MatDialogModule],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe devolver false al cancelar', () => {
    const el = fixture.debugElement.nativeElement.querySelector('button');
    spyOn(component, 'cancel').and.callThrough();

    el.click();

    expect(component.cancel).toHaveBeenCalled();
  });

  it('Debe devolver true al confirmar', () => {
    const el = fixture.debugElement.nativeElement.querySelectorAll('button')[1];
    spyOn(component, 'confirm').and.callThrough();

    el.click();

    expect(component.confirm).toHaveBeenCalled();
  });
});
