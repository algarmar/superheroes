import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { MaterialModule } from './material.module';
import { UppercaseDirective } from './directives/uppercase.directive';

@NgModule({
  declarations: [
    ConfirmDialogComponent,
    UppercaseDirective
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    ConfirmDialogComponent,
    UppercaseDirective
  ]
})
export class SharedModule { }
